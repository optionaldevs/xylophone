//
//  ViewController.swift
//  Xylophone
//
//  Created by Angela Yu on 27/01/2016.
//  Copyright © 2016 London App Brewery. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController{
    
    var xSoundEffect: AVAudioPlayer?
    
    func makePath(index: String) -> URL {
        let url = Bundle.main.url(forResource: "note\(index)", withExtension: "wav")!
        return url
    }
    
    func playSound(url: URL) -> Void {
        do {
            xSoundEffect = try AVAudioPlayer(contentsOf: url)
            xSoundEffect?.play()
        } catch {
            print("exception")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func onClick(_ sender: UIButton) {
        let tagIndex = String(sender.tag)
        let url = self.makePath(index: tagIndex)
        self.playSound(url: url)
    }
}

